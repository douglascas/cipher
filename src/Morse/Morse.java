package Morse;

import java.util.HashMap;

public class Morse {

	public static String alfabeto= "abcdefghijklmnopqrstuvwxyz1234567890";
	
	public static String morse[] = {
		".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
		"....","..",".---","-.-",".-..", "--","-.","---",
		".--.", "--.-", ".-.", "...", "-", "..-", "...-",
		".--", "-..-","-.--","--..", "|",".---","..---",
		"...--","....-",".....","-....","--...","---..",
		"----.","-----"
    };    
	
	public String converteTextoClaroParaMorse(String entrada) {
		entrada = entrada.toLowerCase();
	    String saida = "";
		
	    for (char caractere : entrada.toCharArray()) {
           int index = -1;
           String letra = " ";
           
           for (int i = 0; i < alfabeto.length(); i++) {
        	   if (alfabeto.charAt(i) == caractere) { index = i; }
           }
           
           if (index >= 0) { letra = morse[index]; }
            
           saida += letra + "    ";
        }
	    
		return saida;
	}
	
	public String converteMorseParaTextoClaro(String entrada) {
		String saida = "";
	    String aux = entrada.trim();
	    String[] palavras = aux.split("   ");
	    
	    for (String palavra : palavras) {
	        for(String letter : palavra.split(" ")) {
	            for(int x=0; x < morse.length; x++) {
	                if(letter.equals(morse[x])) {
	                	saida = saida + alfabeto.charAt(x);
	                }
	            }
	        }
	        saida += " ";
	    }
	    
	    return saida.toUpperCase();
    }

}
