package Monoalfabetica;

import java.util.Arrays;

/**
 * Entendemos que a monoalfabética nada mais é do que a substituição de letras.
 * Sendo assim, utilizamos as mesmas regras da Cifra de César, somente alterando 
 * o atributo *regra* para demostração.
 */
public class Monoalfabetica {

	static char[] alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

	static int regra = 1;

	public String cifrar(String textoClaro) {

		textoClaro = textoClaro.toUpperCase();
		char[] textoCifrado = textoClaro.toCharArray();

		// Percorre letra por letra e, se não for espaço em branco, realiza a substituição
		for (int i = 0; i < textoClaro.length(); i++) {
			if (!Character.isWhitespace(textoCifrado[i])) {
				// Avança casas para a direita
				textoCifrado[i] += regra;
			}
		}

		return String.valueOf(textoCifrado);                 
	}

	public String decifrar(String cifra) {

		char[] textoClaro = cifra.toCharArray();

		// Percorre letra por letra e, se não for espaço em branco, realiza a substituição
		for (int i = 0; i < textoClaro.length; i++) {
			if (!Character.isWhitespace(textoClaro[i])) {
				// Avança casas para a esquerda
				textoClaro[i] -= regra;
			}
		}
		
		return String.valueOf(textoClaro);
	}             
	
}