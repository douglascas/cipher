package PlayFair;

import java.awt.Point;
import java.util.Scanner;

public class Playfair {
	private static char[][] matriz;
	private static Point[] posicoes;

	public void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Insira a palavra chave: ");
		String palavraChave = scanner.next();
		
		System.out.print("Insira o texto a ser cifrado: ");
		String textoClaro = scanner.next();
		
		montarMatriz(palavraChave);

		String cifra = cifrar(formatarTexto(textoClaro));
		
		exibirMatriz();
		System.out.printf("Mensagem cifrada: " + cifra + "\n");
		System.out.printf("Mensagem original: " + decifrar(cifra));
		
		scanner.close();
	}

	public String formatarTexto(String texto) {
		texto = texto.toUpperCase().replaceAll("[^A-Z]", "");
		return texto.replace("I", "J");
	}

	public void montarMatriz(String palavraChave) {
		matriz = new char[5][5];
		posicoes = new Point[26];

		String entrada = formatarTexto(palavraChave + "ABCDEFGHIJKLMNOPQRSTUVWXYZ");

		for (int i = 0, k = 0; i < entrada.length(); i++) {
			char caracter = entrada.charAt(i);
			if (posicoes[caracter - 'A'] == null) {
				matriz[k / 5][k % 5] = caracter;
				posicoes[caracter - 'A'] = new Point(k % 5, k / 5);
				k++;
			}
		}
	}
	
	public void exibirMatriz() {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
	}
    // insere x entre algarismos de duas letras e redefine "length"
	public String cifrar(String entrada) {
		StringBuilder stringBuilder = new StringBuilder(entrada);

		for (int i = 0; i < stringBuilder.length(); i += 2) {
			if (i == stringBuilder.length() - 1) {
				stringBuilder.append(stringBuilder.length() % 2 == 1 ? 'X' : "");
			} else if (stringBuilder.charAt(i) == stringBuilder.charAt(i + 1)) {
				stringBuilder.insert(i + 1, 'X');
			}
		}
		
		return substituicaoLetras(stringBuilder, 1);
	}

	public String decifrar(String entrada) {
		return substituicaoLetras(new StringBuilder(entrada), 4);
	}
      // codifica a entrada do dígrafo com as especificações da cifra
	public String substituicaoLetras(StringBuilder texto, int direcao) {
		for (int i = 0; i < texto.length(); i += 2) {
			char a = texto.charAt(i);
			char b = texto.charAt(i + 1);

			int row1 = posicoes[a - 'A'].y;
			int row2 = posicoes[b - 'A'].y;
			int col1 = posicoes[a - 'A'].x;
			int col2 = posicoes[b - 'A'].x;
             // caso 1: as letras no dígrafo são da mesma linha, desloque as colunas para a direita
			if (row1 == row2) {
				col1 = (col1 + direcao) % 5;
				col2 = (col2 + direcao) % 5;
			}// caso 2: as letras no dígrafo são da mesma coluna, desloque as linhas para baixo
			else if (col1 == col2) {
				row1 = (row1 + direcao) % 5;
				row2 = (row2 + direcao) % 5;
			} else // caso 3: letras no retângulo de forma de dígrafo, troque a primeira coluna # pela segunda coluna #
			{
				int tmp = col1;
				col1 = col2;
				col2 = tmp;
			}
            // executa a consulta da tabela e coloca esses valores na matriz codificada
			texto.setCharAt(i, matriz[row1][col1]);
			texto.setCharAt(i + 1, matriz[row2][col2]);
		}
		
		return texto.toString();
	}
}