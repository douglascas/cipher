import java.util.Scanner;

import Cesar.Cesar;
import Monoalfabetica.Monoalfabetica;
import Morse.Morse;
import PlayFair.Playfair;


public class Cifras {

	public static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		boolean programaAtivo = true;
		String entrada;
		
		while (programaAtivo == true) {
			System.out.println("1. Playfair");
			System.out.println("2. Morse");
			System.out.println("3. Cesar");
			System.out.println("4. Vigenere");
			System.out.println("0. Encerrar programa");
			System.out.println();
			System.out.print("Escolha uma opção para começar: ");
			
			entrada = scanner.next();
	
			switch (entrada) {
			case "1":
				executarPlayfair();
				System.out.println();
				break;
			case "2":
				executarMorse();
				System.out.println();
				break;
			case "3":
				executarCesar();
				System.out.println();
				break;
			case "4":
				executarMonoAlfabetica();
				System.out.println();
				break;
			case "0":
				programaAtivo = false;
				break;
			default:
				System.out.println("Opção não encontrada. \n");
				break;
			} 
		} 
		
		System.out.println("Programa encerrado.");
	}
	
	public static void executarPlayfair() {
		Playfair playFair = new Playfair();
		
		System.out.print("Insira a palavra chave: ");
		String palavraChave = scanner.next();
		
		System.out.print("Insira o texto a ser cifrado: ");
		String textoClaro = scanner.next();
		
		playFair.montarMatriz(palavraChave);

		String cifra = playFair.cifrar(playFair.formatarTexto(textoClaro));
		
		playFair.exibirMatriz();
		System.out.println("Mensagem cifrada: " + cifra + "\n");
		System.out.println("Mensagem original: " + playFair.decifrar(cifra));

	}
	
	public static void executarMorse() {
		Morse morse = new Morse();
		
		System.out.println("Escolha uma opção: ");
		System.out.println("1. Texto claro -> morse");
		System.out.println("2. Morse -> Texto claro");

		String texto = scanner.next();
		
		switch (texto) {
		case "1":
			System.out.println("Insira o texto claro a ser convertido em Código Morse: ");
			String textoClaro = scanner.next();
			System.out.println(morse.converteTextoClaroParaMorse(textoClaro));
			break;
		case "2":
			System.out.println("Insira o Código Morse a ser convertido em texto claro: ");
			String inputMorse = scanner.next();
			System.out.println(morse.converteTextoClaroParaMorse(inputMorse));
			break;
		default:
			return;
		}
		
	}
	
	public static void executarCesar() {
		Cesar cesar = new Cesar();
		
		System.out.println("Escolha uma opção: ");
		System.out.println("1. Texto claro -> Cesar");
		System.out.println("2. Cesar -> Texto claro");

		String texto = scanner.next();
		
		switch (texto) {
		case "1":
			System.out.println("Insira o texto claro a ser convertido em Cifra de Cesar: ");
			String textoClaro = scanner.next();
			System.out.println(cesar.cifrar(textoClaro));
			System.out.println(cesar.decifrar(cesar.cifrar(textoClaro)));
			break;
		case "2":
			System.out.println("Insira a Cifra de Cesar a ser convertido em texto claro: ");
			String cifra = scanner.next();
			System.out.println(cesar.decifrar(cifra));
			break;
		default:
			return;
		}
	}
	
	public static void executarMonoAlfabetica() {
		Monoalfabetica monoalfabetica = new Monoalfabetica();
		
		System.out.println("Escolha uma opção: ");
		System.out.println("1. Texto claro -> Mono-alfabética");
		System.out.println("2. Mono-alfabética -> Texto claro");
		String texto = scanner.next();
		
		switch(texto) {
            case "1":
            	System.out.print("Insira o texto a ser decifrado: ");
        		String textoClaro = scanner.next();
    			System.out.println(monoalfabetica.cifrar(textoClaro));
                break;
            case "2":
        		System.out.print("Insira o texto a ser decifrado: ");
        		String cifra = scanner.next();
    			System.out.println(monoalfabetica.decifrar(cifra));
        		break;
        }

	}

}
