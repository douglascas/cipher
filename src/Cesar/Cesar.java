package Cesar;

public class Cesar {

	static char[] alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

	static int regraCesar = 3;

	public String cifrar(String textoClaro) {

		textoClaro = textoClaro.toUpperCase();
		char[] textoCifrado = textoClaro.toCharArray();

		// Percorre letra por letra e, se não for espaço em branco, realiza a substituição
		for (int i = 0; i < textoClaro.length(); i++) {
			if (!Character.isWhitespace(textoCifrado[i])) {
				// Avança 3 casas para a direita
				textoCifrado[i] += regraCesar;
			}
		}

		return String.valueOf(textoCifrado);                 
	}

	public String decifrar(String cifra) {

		char[] textoClaro = cifra.toCharArray();

		// Percorre letra por letra e, se não for espaço em branco, realiza a substituição
		for (int i = 0; i < textoClaro.length; i++) {
			if (!Character.isWhitespace(textoClaro[i])) {
				// Avança 3 casas para a esquerda
				textoClaro[i] -= regraCesar;
			}
		}
		
		return String.valueOf(textoClaro);
	}             

}